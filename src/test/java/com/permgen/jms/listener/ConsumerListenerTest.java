package com.permgen.jms.listener;

import javax.jms.TextMessage;
import org.junit.After;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author koman
 */
public class ConsumerListenerTest {
    
    private TextMessage textMessage;
    
    public ConsumerListenerTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testOnMessage() {
        
        ConsumerListener listener = new ConsumerListener();
        listener.onMessage(textMessage);
        assertNull(textMessage);
    }
}

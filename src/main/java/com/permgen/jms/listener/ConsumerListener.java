package com.permgen.jms.listener;

import javax.jms.Message;
import javax.jms.MessageListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author koman
 */
@Component
public class ConsumerListener implements MessageListener {

    @Override
    public void onMessage(Message msg) {
        System.out.println("In OnMessage");
    }
    
}
